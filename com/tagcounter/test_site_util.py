import responses
import requests
import site_util


@responses.activate
def test_read():
    test_response_body = '''
    <body>
    <p class="title">
        <b>Body's title</b>
    </p>
    <p class="story">line begins
        <a class="element" href="http://example.com/element1" id="link1">1</a>
        <a class="element" href="http://example.com/element2" id="link2">2</a>
        <a class="element" href="http://example.com/element3" id="link3">3</a>
        <p> line ends</p>
    </p>
    </body>
    <b>Body's title</b>
    '''

    test_url = 'http://test.com'

    responses.add(responses.GET, test_url, body=test_response_body, status=404)
    html_text = requests.get(test_url, verify=False, timeout=3).text

    assert test_response_body == html_text
    assert site_util.read(test_url) == {'a': 3, 'b': 2, 'body': 1, 'p': 3}

