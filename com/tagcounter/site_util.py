import requests
from bs4 import BeautifulSoup
from collections import Counter


def read(url):
    try:
        html_text = requests.get(url, verify=False, timeout=3).text

        soup = BeautifulSoup(html_text, 'html.parser')
        tags = [tag.name for tag in soup.find_all()]

        return dict(Counter(tags))

    except Exception as e:
        return None
