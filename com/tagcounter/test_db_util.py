from datetime import date

from db_util import *
import pytest


@pytest.fixture
def prepare_db():
    conn = init(in_memory_db=True)
    yield conn
    conn.close()


def test_insert(prepare_db):
    conn = prepare_db
    c = conn.cursor()
    key = "test.com"
    test_object = (key, f"http://{key}", str(date.today()), {})

    insert(conn, test_object)
    c.execute(select_all_string, (key,))
    result = c.fetchone()
    c.close()
    assert result == test_object


def test_update(prepare_db):
    conn = prepare_db
    c = conn.cursor()
    key = "test2.com"
    test_object = (key, f"http://{key}", str(date.today()), {})
    update_object = (f"http://{key}", str(date.today()), {"body:1"}, key)
    expected_object = (key, f"http://{key}", str(date.today()), {"body:1"})

    insert(conn, test_object)
    update(conn, update_object)
    c.execute(select_all_string, (key,))
    result = c.fetchone()
    c.close()
    assert result == expected_object


def test_select_all(prepare_db):
    conn = prepare_db
    c = conn.cursor()
    key = "test3.com"
    expected_object = (key, f"http://{key}", str(date.today()), {})

    insert(conn, expected_object)
    result = select_all(conn, (key,))
    c.close()
    assert result == expected_object


def test_select_tags(prepare_db):
    conn = prepare_db
    c = conn.cursor()
    key = "test3.com"
    expected_tags = {"body:2", "head:2"}
    test_object = (key, f"http://{key}", str(date.today()), expected_tags)

    insert(conn, test_object)
    result = select_tags(conn, (key,))
    c.close()
    assert result == expected_tags
