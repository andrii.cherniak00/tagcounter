from unittest.mock import patch, mock_open

import dict_util


@patch("builtins.open", new_callable=mock_open, read_data="key1: value1")
def test_patch(mock_file):
    assert open("path/to/open").read() == "key1: value1"
    mock_file.assert_called_with("path/to/open")

    assert dict_util.get_full_name("NotExistingKey") == "NotExistingKey"
    assert dict_util.get_full_name("key1") == "value1"
