import argparse
import logging
from datetime import date

import tkinter as tk

import db_util
import site_util
import dict_util
import gui

print_to_console = True


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--get', help='Get tags by url', required=False)
    parser.add_argument('-v', '--view', help='View saved tags', required=False)
    return parser.parse_args()


def log_request(request):
    logging.basicConfig(filename='get_requests.log', filemode='a', format='%(asctime)s %(message)s')
    logging.warning(request)


def get_site(site_name):
    site = dict_util.get_full_name(site_name)
    log_request(site)

    url = f"http://{site}"
    tags = site_util.read(url)

    if not tags:
        return "Site not found"

    conn = db_util.init()
    some_object = (site, url, date.today(), tags)
    db_util.insert(conn, some_object)
    conn.close()

    return tags


def view_site(site_name):
    site = dict_util.get_full_name(site_name)
    conn = db_util.init()
    result = db_util.select_tags(conn, (site,))
    conn.close()

    return result


def start_gui():
    root = tk.Tk()
    myapp = gui.GUI(root)
    myapp.mainloop()


if __name__ == '__main__':
    args = parse_args()
    if args.get:
        print("Get command. Argument=" + args.get)
        print(get_site(args.get))
    elif args.view:
        print("View command. Argument=" + args.view)
        print(view_site(args.view))
    else:
        print("Starting GUI")
        start_gui()
