import sqlite3
import pickle

table_name = "data"
insert_string = "INSERT OR REPLACE into %s values (?, ?, ?, ?)" % table_name
update_string = "UPDATE %s SET url=?, check_date=?, tags=? WHERE site=?" % table_name
select_all_string = "SELECT site, url, check_date, tags FROM %s WHERE site=?" % table_name
select_tags_string = "SELECT tags FROM %s WHERE site=?" % table_name
create_table = """
                CREATE TABLE IF NOT EXISTS %s (
                    "site"	TEXT NOT NULL UNIQUE,
                    "url"	TEXT NOT NULL,
                    "check_date"	TEXT NOT NULL,
                    "tags"	pickle NOT NULL,
                    PRIMARY KEY("site")
                    )""" % table_name


def init(in_memory_db=False):
    protocol = 0
    sqlite3.register_converter("pickle", pickle.loads)
    sqlite3.register_adapter(list, pickle.dumps)
    sqlite3.register_adapter(set, pickle.dumps)
    sqlite3.register_adapter(dict, pickle.dumps)
    connection = create_schema(in_memory_db)
    return connection


def create_schema(in_memory_db):
    try:
        if in_memory_db:
            conn = sqlite3.connect(':memory:', detect_types=sqlite3.PARSE_DECLTYPES)
        else:
            conn = sqlite3.connect('example.db', detect_types=sqlite3.PARSE_DECLTYPES)

        c = conn.cursor()
        c.execute(create_table)

    except sqlite3.OperationalError:
        pass

    return conn


def insert(conn, obj):
    try:
        c = conn.cursor()
        c.execute(insert_string, obj)
        c.close()
        conn.commit()
    except sqlite3.IntegrityError:
        print("Duplicate key")


def update(conn, obj):
    c = conn.cursor()
    c.execute(update_string, obj)
    conn.commit()


def select_all(conn, key):
    try:
        c = conn.cursor()
        c.execute(select_all_string, key)
        data = c.fetchone()
        c.close()
        return data

    except Exception as e:
        return "Site not found"


def select_tags(conn, key):
    try:
        c = conn.cursor()
        c.execute(select_tags_string, key)
        data = c.fetchone()
        c.close()
        return data[0]

    except Exception as e:
        return "Site not found"
