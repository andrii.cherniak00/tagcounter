import json
import time
import tkinter
import tkinter as tk
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText

from tagcounter import get_site, view_site
import dict_util


class GUI(tk.Frame):
    def __init__(self, master: tk.Tk):
        super().__init__(master)
        master.title("Myapp")
        self.grid()

        width = 80
        self.default_message = "Enter URL ..."

        self.entry = tk.Entry(self, width=width)
        self.entry.insert(0, self.default_message)
        self.entry.bind('<FocusIn>', self.on_entry_click)
        self.entry.bind('<FocusOut>', self.on_focusout)
        self.entry.config(fg='grey')
        self.entry.grid(column=0, row=0, columnspan=2, padx=0)

        self.get_button = ttk.Button(self, text="Get", command=self.get, width=int(width / 2) - 2)
        self.get_button.grid(column=0, row=1)

        self.view_button = ttk.Button(self, text="View", command=self.view, width=int(width / 2))
        self.view_button.grid(column=1, row=1)

        self.text = ScrolledText(self, width=width - 20, height=10)
        self.text.grid(column=0, row=2, columnspan=2)

        self.view_synonyms_button = ttk.Button(self, text="View synonyms", command=self.view_synonyms,
                                               width=int(width / 2) - 2)
        self.view_synonyms_button.grid(column=0, row=3)

        self.save_synonyms_button = ttk.Button(self, text="Save synonyms", command=self.save_synonyms,
                                               width=int(width / 2))
        self.save_synonyms_button.grid(column=1, row=3)

        self.statusbar = tk.Label(self, text="on the way…", width=width - 20)
        self.statusbar.grid(column=0, row=4, columnspan=2)

    def update_status_bar(self, message):
        self.statusbar.configure(text=message)

    def get(self):
        start = time.time()
        site = self.entry.get()
        if site:
            response = get_site(site)
            formatted_response = json.dumps(response, indent=2)
            end = time.time()
            response_time = round((end - start) * 1000)

            self.print_text(formatted_response)
            self.update_status_bar(f"Response time: {response_time}ms")
        else:
            self.update_status_bar("empty")

    def view(self):
        start = time.time()
        response = view_site(self.entry.get())
        formatted_response = json.dumps(response, indent=2)
        end = time.time()
        response_time = round((end - start) * 1000)

        self.print_text(formatted_response)
        self.update_status_bar(f"Response time: {response_time}ms")

    def view_synonyms(self):
        data = dict_util.get_synonyms_from_file()
        self.print_text(data)

    def save_synonyms(self):
        dict_util.save_synonyms_to_file(self.text.get(1.0, tk.END))

    def print_text(self, text):
        self.text.delete(1.0, tk.END)
        self.text.insert(tkinter.INSERT, text)
        # self.text.update_idletasks()

    def on_entry_click(self, event):
        """function that gets called whenever entry is clicked"""
        if self.entry.get() == self.default_message:
            self.entry.delete(0, "end")  # delete all the text in the entry
            self.entry.insert(0, '')  # Insert blank for user input
            self.entry.config(fg='black')

    def on_focusout(self, event):
        if self.entry.get() == '':
            self.entry.insert(0, self.default_message)
            self.entry.config(fg='grey')
