import yaml

file_name = "synonyms.yaml"


def get_full_name(short_name):
    with open(file_name) as stream:
        try:
            data: dict = yaml.safe_load(stream)
            return data.get(short_name, short_name)

        except yaml.YAMLError as exc:
            print(exc)


def get_synonyms_from_file():
    with open(file_name) as f:
        return f.read()


def save_synonyms_to_file(data):
    with open(file_name, "w") as f:
        f.write(data)
